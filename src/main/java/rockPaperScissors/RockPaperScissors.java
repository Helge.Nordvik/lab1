package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    


    public void run() {
        // TODO: Implement Rock Paper Scissors
        // Source: https://www.javatpoint.com/how-to-take-string-input-in-java
        // Spør etter input fra spiller
        //int roundCounter2 = 1;
        String str1 = Integer.toString(roundCounter);
        Scanner sc= new Scanner(System.in);
        //System.out.print("Let's play round 1\n Your choice (Rock/Paper/Scissors): ");  
        System.out.print("Let's play round " +str1 + "\n Your choice (rock/paper/scissors)?\n");
        String str= sc.nextLine();              
        
        if(!str.equals("rock") && !str.equals("paper") && !str.equals("scissors")) {

            System.out.println("I do not understand " + str +". Could you try again?");

            new RockPaperScissors().run();
        
        } else {

            //Forsøk på å randomisere hva computer velger

            final String[] rpsChoices2 = {"rock", "paper", "scissors"};
            Random random = new Random();
            int index = random.nextInt(rpsChoices2.length);
            String compStr = rpsChoices2[index];
            
        
            
            
            //System.out.print("Human choose "+str +", computer choose: "+compStr +"!\n"); Denne laget jeg for å se hvordan jeg kunne få legge input   
            //sammenligne de to valgene gjennom if/elif/else. Den første biten er for å se om de er like.
            if (str.equals(compStr)) {
                System.out.println("Human chose "+str +", computer chose "+compStr +". It's a tie!"); //Her ser vi om spilleren vinner
                System.out.println("Score: Human " + humanScore + ", computer " + computerScore); 

            } else if ("rock".equals(str) && "scissors".equals(compStr)){
                System.out.println("Human chose "+str +", computer chose "+compStr +". Human wins!");
                humanScore++;
                humanScore += 1;
                System.out.println("Score: Human " + humanScore + ", computer " + computerScore);

            } else if ("scissors".equals(str) && "paper".equals(compStr)){
                System.out.println("Human chose "+str +", computer chose "+compStr +". Human wins!");
                humanScore++;
                humanScore += 1;
                System.out.println("Score: Human " + humanScore + ", computer " + computerScore);

            } else if ("paper".equals(str) && "rock".equals(compStr)){
                System.out.println("Human chose "+str +", computer chose "+compStr +". Human wins!");    
                humanScore++;
                humanScore += 1;
                System.out.println("Score: Human " + humanScore + ", computer " + computerScore);
                
                //Tilsvarende kode for datamaskinen er ikke nødvendig da det er eneste muligheter

            } else {
                System.out.println("Human chose "+str +", computer chose "+compStr +". Computer wins!");
                computerScore++;
                computerScore += 1;
                System.out.println("Score: Human " + humanScore + ", computer " + computerScore);
            }
            
            Scanner sc2 = new Scanner(System.in);
            System.out.print("Do you wish to continue playing? (y/n)\n");  
            String quit = sc2.nextLine();  
            if ("y".equals(quit)){
                //System.out.println("Really?"); Var en test for å se om det fungerte.
                roundCounter += 1;
                new RockPaperScissors().run();
                //RockPaperScissors().run();
            } else {
                System.out.println("Bye bye :)");
            }      
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
